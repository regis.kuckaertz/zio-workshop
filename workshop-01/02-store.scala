//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"
//> using lib "software.amazon.awssdk:s3:2.17.233"
//> using lib "software.amazon.awssdk:dynamodb:2.17.233"

package workshop1

import zio._
import zio.stream._
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider
import software.amazon.awssdk.services.s3.model.S3Exception
import software.amazon.awssdk.core.exception.SdkException
import software.amazon.awssdk.services.s3.S3AsyncClient
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient
import java.net.http.HttpClient
import software.amazon.awssdk.services.dynamodb.model.BatchGetItemRequest
import software.amazon.awssdk.regions.providers.DefaultAwsRegionProviderChain
import org.apache.http.client.CredentialsProvider
import software.amazon.awssdk.auth.credentials.DefaultCredentialsProvider
import scala.jdk.CollectionConverters._

case class S3Config(
    region: Region,
    credentialsProvider: AwsCredentialsProvider
)

object S3 extends Utils {
  val openFromConfig: ZIO[S3Config, SdkException, S3AsyncClient] =
    for {
      s3Config <- ZIO.service[S3Config]
      s3Client <- ZIO
        .attempt(
          S3AsyncClient
            .builder()
            .region(s3Config.region)
            .credentialsProvider(s3Config.credentialsProvider)
            .build()
        )
        .refineToOrDie[SdkException]
    } yield s3Client

  val s3: ZLayer[S3Config, SdkException, S3AsyncClient] =
    ZLayer.scoped {
      ZIO.acquireRelease(openFromConfig)(close)
    }

  val dynamodb: ZLayer[S3Config, SdkException, DynamoDbAsyncClient] =
    ZLayer.scoped {
      ???
    }

  val s3Config: ULayer[S3Config] =
    ZLayer.succeed(
      S3Config(
        Region.EU_CENTRAL_1,
        DefaultCredentialsProvider.create()
      )
    )

  def ex2: ZIO[
    S3Config,
    SdkException,
    Int
  ] =
    (for {
      s3Client <- ZIO.service[S3AsyncClient]

      total <- Ref.make(0)

      bucketResponse <- s3Call(s3Client.listBuckets())

      _ <- processBuckets(s3Client, total, bucketResponse)

      result <- total.get
    } yield result)
      .provide(
        s3,
        s3Config
      )
}

trait Store {
  def ls: Task[List[String]]

  def isDirectory(name: String): Task[Boolean]

  def cd(name: String): Task[Unit]

  def mkdir(name: String): Task[Unit]
}

object Store {
  def ls: ZIO[Store, Throwable, List[String]] =
    ZIO.serviceWithZIO(_.ls)

  def isDirectory(name: String): RIO[Store, Boolean] =
    ZIO.serviceWithZIO(_.isDirectory(name))

  def cd(name: String): ZIO[Store, Throwable, Unit] =
    ZIO.serviceWithZIO(_.cd(name))

  def mkdir(name: String): ZIO[Store, Throwable, Unit] =
    ZIO.serviceWithZIO(_.mkdir(name))
}

/** Exercise 1. Create a layer that provides an implementation of the Store
  * trait that uses the S3 API.
  */

import software.amazon.awssdk.services.s3._
import software.amazon.awssdk.services.s3.model._

object Store1 extends ZIOAppDefault with Utils {
  class StoreImpl(s3Client: S3AsyncClient) extends Store {
    def cd(name: String): Task[Unit] = ZIO.unit

    def isDirectory(name: String): Task[Boolean] = ZIO.succeed(false)

    def ls: Task[List[String]] =
      s3Call(s3Client.listBuckets())
        .flatMap(response =>
          ZIO
            .foreach(response.buckets().asScala.to(List))(bucket =>
              s3Call(
                s3Client.listObjects(
                  ListObjectsRequest.builder().bucket(bucket.name()).build()
                )
              )
            )
            .map(_.flatMap(_.contents.asScala.to(List)).map(_.key()))
        )

    def mkdir(name: String): Task[Unit] = ZIO.unit
  }

  val fromS3: ZLayer[Any, Throwable, Store] =
    ZLayer.scoped {
      ZIO.acquireRelease(open)(close).map(new StoreImpl(_))
    }

  val run = {
    def go: RIO[Store, Unit] =
      for {
        _ <- ZIO.logInfo("Listing current directory")
        names <- Store.ls
        _ <- ZIO.foreachDiscard(names)(f =>
          for {
            _ <- Console.printLine(s"  - $f")
            _ <- ZIO.ifZIO(Store.isDirectory(f))(Store.cd(f) *> go, ZIO.unit)
          } yield ()
        )
      } yield ()

    go.provide(fromS3)
  }
}

/** Exercise 2. S3 does not have the concept of directories. Create a layer that
  * provides an implementation of the Store trait that uses the S3 API but that
  * also has the concept of directories.
  */

object Store2 {
  val fromS3: ZLayer[Any, Throwable, Store] = ???

  val run =
    (
      for {
        _ <- ZIO.logInfo("Listing current directory")
        names <- Store.ls
        _ <- ZIO.foreachDiscard(names)(f => Console.printLine(s"  - $f"))
      } yield ()
    ).provide(fromS3)
}

/** Exercise 3. Imagine we need to use other AWS services. All APIs from the SDK
  * use an HTTP client, which by default is created on the fly. It would be such
  * a waste to have as many HTTP clients as we have layers.
  *
  * Create a layer that provides an implementation of the Store trait but
  * requires the http client to be provided by some other layer.
  */
