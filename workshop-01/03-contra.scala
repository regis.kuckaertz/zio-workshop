//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"
//> using lib "software.amazon.awssdk:s3:2.17.233"

package workshop1

import zio._
import zio.stream._

object Contra {
  val f: Int => String =
    (x: Int) => s"Hey! Check this out: $x"

  val absurd: () => String =
    () => "Hoy"

  sealed trait SIO[-in, +out] {
    def provide(x: in): SIO[Any, out] =
      ProvideRequirement(x, this)

    def contramap[in0](f: in0 => in): SIO[in0, out] =
      this match {
        case NoRequirement(f)            => NoRequirement(f)
        case ProvideRequirement(in, sio) => ProvideRequirement(in, sio)
        case WithRequirement(g)          => WithRequirement(g.compose(f))
      }
  }

  case class NoRequirement[out](f: () => out) extends SIO[Any, out]
  case class WithRequirement[in, out](f: in => out) extends SIO[in, out]
  case class ProvideRequirement[in, out](in: in, sio: SIO[in, out])
      extends SIO[Any, out]

  val sio = WithRequirement(f).provide(10)

  def execute[out](sio: SIO[Any, out]): out = ???

  execute(sio)
}
