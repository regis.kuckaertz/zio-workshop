//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"
//> using lib "software.amazon.awssdk:s3:2.17.233"

package workshop1

import zio._
import zio.stream._

/** Exercise 1. Write a workflow that does the following:
  *
  *   - instantiate an S3 *asynchronous* client
  *   - list all the buckets in the account (s3Client.listBuckets) and print
  *     their name to the console
  *   - for each bucket, list all the objects in the bucket
  *     (s3Client.listObjectsV2) and print their name to the console
  *   - close the client
  *   - yields the collective total of objects overall
  *
  * API documentation: https://sdk.amazonaws.com/java/api/latest/
  */

import software.amazon.awssdk.services.s3._
import software.amazon.awssdk.services.s3.model._
import java.util.concurrent.CompletableFuture
import scala.jdk.CollectionConverters._

trait Utils {
  val open =
    Console.printLine("Creating S3 client").orDie *> ZIO
      .attempt(S3AsyncClient.builder().build())
      .refineToOrDie[S3Exception]

  def close(s3: S3AsyncClient) =
    Console.printLine("Closing S3 client").orDie *> ZIO
      .succeed(s3.close())

  def s3Call[b](f: => CompletableFuture[b]): ZIO[Any, S3Exception, b] =
    ZIO.fromCompletableFuture(f).refineToOrDie[S3Exception]

  def processBuckets(
      s3Client: S3AsyncClient,
      total: Ref[Int],
      response: ListBucketsResponse
  ) =
    ZIO.foreach(response.buckets().asScala.to(List))(b =>
      for {
        _ <- Console.printLine(s"* ${b.name()}").orDie
        objectsResponse <- ZIO
          .fail(S3Exception.builder().message("Boom!").build())
          .refineToOrDie[S3Exception] *> s3Call(
          s3Client.listObjects(
            ListObjectsRequest.builder().bucket(b.name).build()
          )
        )
        _ <- ZIO.foreach(objectsResponse.contents().asScala.to(List))(o =>
          Console.printLine(s"  - ${o.key()}").orDie *> total.update(_ + 1)
        )
      } yield ()
    )

}

object Exercise1 extends ZIOAppDefault with Utils {
  def ex1: ZIO[Any, S3Exception, Int] =
    for {
      s3Client <- open

      total <- Ref.make(0)

      bucketResponse <- s3Call(s3Client.listBuckets())

      _ <- processBuckets(s3Client, total, bucketResponse)

      _ <- ZIO.succeed(s3Client.close())

      result <- total.get
    } yield result

  val run =
    (for {
      cnt <- ex1
      _ <- Console.printLine(s"There were $cnt objects in all buckets")
    } yield ())
      .catchAllCause(cause => Console.printLine(cause.prettyPrint))
      .exitCode
}

/** Exercise 2. The program above has a big problem! If there is an error
  * somewhere, the s3 client will not be closed, which may result in a memory
  * leak!
  *
  * Can you rewrite the program above so that the s3 client is closed if it
  * fails?
  */

object Exercise2 extends ZIOAppDefault with Utils {
  def ex2: ZIO[Any, S3Exception, Int] =
    ZIO.scoped {
      for {
        s3Client <- ZIO.acquireRelease(open)(close)

        total <- Ref.make(0)

        bucketResponse <- s3Call(s3Client.listBuckets())

        _ <- processBuckets(s3Client, total, bucketResponse)

        result <- total.get
      } yield result
    }

  val run =
    (for {
      cnt <- ex2
      _ <- Console.printLine(s"There were $cnt objects in all buckets")
    } yield ()).exitCode
}

/** Exercise 3. It is very unlikely you have handled all the situations which
  * can lead to a memory leak above. Fortunately, ZIO has an answer for that:
  * scopes!
  *
  * Introduce a scope to manage the lifetime of the s3 client.
  */

object Exercise3 extends ZIOAppDefault {
  def ex3: ZIO[Scope, S3Exception, Int] = ???

  val run =
    (for {
      cnt <- ex3
      _ <- Console.printLine(s"There were $cnt objects in all buckets")
    } yield ()).exitCode
}
