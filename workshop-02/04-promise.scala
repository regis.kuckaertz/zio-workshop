//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"

package workshop2

import zio._
import java.util.concurrent.TimeUnit

trait Api {
  def query(req: Int): Task[String]
}

object Api {
  def query(req: Int): RIO[Api, String] =
    ZIO.serviceWithZIO(_.query(req))

  type Cache = Map[Int, Promise[Throwable, String]]

  val test: ULayer[Api] =
    ZLayer.fromZIO {
      Ref.Synchronized.make[Cache](Map.empty).map { memo =>
        new Api {
          def callSearchEngine(req: Int): Task[String] =
            Random
              .nextIntBetween(1, 5)
              .flatMap(s =>
                Clock.localDateTime
                  .flatMap(dt => Console.printLine(dt.toString())) *> ZIO
                  .succeed(s"Result is $req")
                  .delay(Duration(s, TimeUnit.SECONDS)) <* Clock.localDateTime
                  .flatMap(dt => Console.printLine(dt.toString()))
              )

          def query(req: Int): Task[String] =
            memo
              .modifyZIO(map =>
                map.get(req) match {
                  case None =>
                    Console.printLine(s"Searching for $req...") *> Promise
                      .make[Throwable, String]
                      .tap(p => callSearchEngine(req).intoPromise(p).fork)
                      .map(p => p.await -> map.updated(req, p))

                  case Some(p) =>
                    ZIO.succeed(p.await -> map)
                }
              )
              .flatten
        }
      }
    }
}

/** Exercise 1. Design a system that allows any number of client to call an API
  * such that the downstream system is not overloaded. The first port of call is
  * to make sure duplicate requests are deduplicated at the call site. How can
  * you do that?
  */
object Promise1 extends ZIOAppDefault {

  val run =
    ZIO
      .forkAll(
        List.fill(10)(
          Console.printLine(s"Looking up 0") *> Api
            .query(0)
            .flatMap(res => Console.printLine("Result is " + res))
        )
      )
      .flatMap(_.join)
      .provide(Api.test)
}
