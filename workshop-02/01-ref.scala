//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-concurrent:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"

package workshop2

import zio._
import zio.stream.ZStream
import scala.annotation.tailrec
import zio.concurrent.ReentrantLock

/** Exercise 1. Fibonacci is the classic example where the tree of computations
  * lends itself to a dynamic programming interpretation. Write a version of
  * Fbonacci that memoizes previous results. Use a `Ref[?]` to store the
  * memoized values. fib n = fib (n - 2) + fib (n - 1)
  */
object Exercise1 extends ZIOAppDefault {
  def fib(n: Int): BigInt = {
    @tailrec def go(n: Int, x: BigInt, y: BigInt): BigInt =
      n match {
        case 0 => x
        case 1 => y
        case n => go(n - 1, y, x + y)
      }

    go(n, 0, 1)
  }

  def fibZIO(
      memo: Ref[Map[Int, BigInt]],
      lock: ReentrantLock
  ): Int => UIO[BigInt] =
    i =>
      for {
        _ <- lock.lock
        maybeO <- memo.get.map(_.get(i))
        result <- maybeO match {
          case None =>
            Console.printLine(s"Doing $i").ignore *> fibZIO(memo, lock)(i - 2)
              .zipWith(fibZIO(memo, lock)(i - 1))(_ + _)
              .tap(o => memo.update(_.updated(i, o)))
          case Some(o) =>
            ZIO.succeed(o)
        }
        _ <- lock.unlock
      } yield result

  val run =
    for {
      _ <- Console.printLine("Starting: ")
      memo <- Ref.make(Map[Int, BigInt](0 -> 0, 1 -> 1))
      lock <- ReentrantLock.make()
      avgDuration <- ZStream
        .fromIterable(0 to 100)
        .mapZIO(i => fibZIO(memo, lock)(i).timed)
        .runFold(0.second) { case (avg, (d, n)) =>
          (avg + d).dividedBy(2)
        }
      _ <- Console.printLine(
        s"Average time to compute fibonacci number: ${avgDuration.getNano() / 1000}μs"
      )
    } yield ()
}

/** Exercise 2. The program above has a major flaw. It does not guarantee that
  * the computation of `fib(n)` for any n will happen only once. Use a
  * `Ref.Synchronized` to address this problem.
  */

object Ref2 extends ZIOAppDefault {
  def fibZIO(
      memo: Ref[Map[Int, BigInt]],
      lock: ReentrantLock
  ): Int => UIO[BigInt] =
    i =>
      for {
        _ <- lock.lock
        maybeO <- memo.get.map(_.get(i))
        result <- maybeO match {
          case None =>
            Console.printLine(s"Doing $i").ignore *> fibZIO(memo, lock)(i - 2)
              .zipWith(fibZIO(memo, lock)(i - 1))(_ + _)
              .tap(o => memo.update(_.updated(i, o)))
          case Some(o) =>
            ZIO.succeed(o)
        }
        _ <- lock.unlock
      } yield result

  val run =
    for {
      _ <- Console.printLine("Starting: ")
      memo <- Ref.Synchronized.make(Map[Int, BigInt](0 -> 0, 1 -> 1))
      lock <- ReentrantLock.make()
      avgDuration <- ZStream
        .fromIterable(0 to 100)
        .mapZIO(i => fibZIO(memo, lock)(i).timed)
        .runFold(0.second) { case (avg, (d, n)) =>
          (avg + d).dividedBy(2)
        }
      _ <- Console.printLine(
        s"Average time to compute fibonacci number: ${avgDuration.getNano() / 1000}μs"
      )
    } yield ()
}

/** Exercise 3. Another way of looking at the Fibonacci series is a map-reduce
  * problem. Create a tree of concurrent computations all working on the same
  * `FiberRef` to store their results. Use parent-propagation for the reduce
  * part of the algorithm.
  */

object Exercise3 extends ZIOAppDefault {

  def fibZIO(ref: FiberRef[BigInt]): Int => UIO[Unit] = {
    case 0 => ref.set(0)
    case 1 => ref.set(1)
    case n =>
      for {
        f1 <- fibZIO(ref)(n - 2).fork
        f2 <- fibZIO(ref)(n - 1).fork
        _ <- Fiber.joinAll(List(f1, f2))
      } yield ()
  }

  val run =
    for {
      _ <- Console.printLine("Starting...")
      res <- FiberRef.make(
        BigInt(0),
        fork = (_: BigInt) => BigInt(0),
        join = (x: BigInt, y: BigInt) => x + y
      )
      avgDuration <- ZStream
        .fromIterable(0 to 20)
        .mapZIO(i => res.set(0) *> fibZIO(res)(i).timed)
        .runFold(0.second) { case (avg, (d, n)) =>
          (avg + d).dividedBy(2)
        }
      _ <- Console.printLine(
        s"Average time to compute fibonacci number: ${avgDuration.getNano() / 1000}μs"
      )
    } yield ()
}
