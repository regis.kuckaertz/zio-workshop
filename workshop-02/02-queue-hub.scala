//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"

package workshop2

import zio._
import scala.annotation.nowarn

trait QueueExercise {
  @nowarn("msg=outer")
  final case class MouseEvent(eventId: Int, userId: Int, x: Int, y: Int)

  val start = (0, 100, 100)

  def mouseEvents(queue: Queue[MouseEvent]) =
    for {
      ref <- Ref.Synchronized.make(start)
      _ <- ref
        .updateAndGetZIO { case (offset, x, y) =>
          ZIO.succeed(offset + 1) <*> Random.nextIntBetween(
            x - 5,
            x + 5
          ) <*> Random.nextIntBetween(
            y - 5,
            y + 5
          )
        }
        .tap(coords =>
          queue.offer(MouseEvent(coords._1, 1, coords._2, coords._3))
        )
        .repeat(Schedule.spaced(20.milliseconds).jittered)
    } yield ()

}

/** Exercise 1. Imagine a site was tracking ever action you take on the site.
  * Not hard to imagine 😂 Write the code that handles mouse events from
  * multiple users. There can be any number of users, but let's say there are 10
  * and that number never changes. They all mouse the mouse at random times.
  * Your job is to keep track of the last position of each user. The tracket
  * needs to handle events without overloading the system. It's ok if events are
  * dropped as long as we always get the latest data. We want to keep track of
  * the average lag, that would be useful to scale up if needed.
  */

object Queue1 extends ZIOAppDefault with QueueExercise {

  type UserId = Int
  type EventId = Int
  type Position = (Int, Int)

  def track(pos: Ref[Map[UserId, Position]], avgLag: Ref[(EventId, Double)])(
      evt: MouseEvent
  ): UIO[Double] =
    pos.update(_.updated(evt.userId, (evt.x, evt.y))) *> avgLag
      .updateAndGet { case (lastEvtId, lag) =>
        (evt.eventId, lag + (evt.eventId - lastEvtId) / 2)
      }
      .map(_._2)
      .delay(500.millis)

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    (for {
      queue <- ZIO.acquireRelease(Queue.sliding[MouseEvent](10))(_.shutdown)
      positions <- Ref.make(Map.empty[UserId, Position])
      avgLag <- Ref.make(0 -> 1d)
      producer <- mouseEvents(queue).fork
      consumer <- queue.take
        .flatMap(track(positions, avgLag))
        .flatMap(lag => Console.printLine(s"Average lag is $lag"))
        .forever
    } yield ()).timeout(10.seconds)
}

/** Exercise 2. Acme corp just had another idea: they want to know how active
  * users are on their website. We need anothe component that will give users a
  * score. For simplicity's sake, lets use the average movement time as our
  * metric. Can you add that component that acts in conjunction with the one
  * tracking mouse position?
  */

object Queue2 extends ZIOAppDefault with QueueExercise {

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] = ???
}
