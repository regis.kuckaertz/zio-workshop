//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.0"
//> using lib "dev.zio::zio-streams:2.0.0"
//> using lib "dev.zio::zio-test:2.0.0"

package workshop2

import zio._
import java.util.concurrent.TimeUnit

trait Db {
  def get(id: Int): Task[Option[String]]
  def put(id: Int, value: String): Task[Unit]
}

object Db {
  def get(id: Int): RIO[Db, Option[String]] =
    ZIO.serviceWithZIO(_.get(id))

  def put(id: Int, value: String): RIO[Db, Unit] =
    ZIO.serviceWithZIO(_.put(id, value))

  final val test: ZLayer[Any, Nothing, Db] =
    ZLayer.fromZIO {
      Ref.make(Map.empty[Int, String]).map { ref =>
        new Db {
          def get(id: Int): Task[Option[String]] =
            ref.get.map(_.get(id))
          def put(id: Int, value: String): Task[Unit] =
            ref.update(_.updated(id, value))
        }
      }
    }

}

/**
 * Exercise 1. Sometimes the only thing you can do is poll. We want to get a
 * specific item from the database, id 50. Write a consumer that constantly
 * polls the database. In order not to overload the database, it should only
 * slowly ramp down the polling rate (the fibonacci sequence is a nice way to do
 * that). But it should not go beyond 10 seconds: once it reaches that
 * threshold, it should keep on polling at a fixed rate.
 *
 * At the end, the consumer should log the number of repetitions it did before
 * it finally got the item.
 */
object Schedule1 extends ZIOAppDefault {

  val producer =
    (for {
      i <- Random.nextIntBetween(0, 100)
      now <- Clock.currentTime(TimeUnit.SECONDS)
      s <- if (now % 5 == 0) ZIO.succeed(s"$i") else Random.nextString(10)
      _ <- Db.put(i, s)
    } yield ()).schedule(Schedule.fixed(500.millis))

  val consumer = ???

  val program =
    producer <&> consumer

  val run = program.provide(Db.test)
}
