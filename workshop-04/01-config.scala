//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-config:3.0.2"
//> using lib "dev.zio::zio-config-typesafe:3.0.2"

package workshop4

import zio._
import zio.config._
import zio.config.ConfigDescriptor._
import java.nio.file.Path
import java.nio.file.Paths

/** Exercise 1: take 3 random environment variables from your local setup, parse
  * them into a value of `Config` below, and print the result.
  */
object Config1 extends ZIOAppDefault {
  case class Config(value1: String, values2: String, value3: String)

  /** vscode { git { askpass { node = blabl extra.args = ttte main = test } } }
    */

  val config: ConfigDescriptor[Config] =
    ConfigDescriptor
      .string("VSCODE_GIT_ASKPASS_NODE")
      .zip(ConfigDescriptor.string("VSCODE_GIT_ASKPASS_EXTRA_ARGS"))
      .zip(ConfigDescriptor.string("VSCODE_GIT_ASKPASS_MAIN"))
      .to[Config]

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    zio.config
      .read(config.from(ConfigSource.fromSystemEnv(keyDelimiter = Some('_'))))
      .flatMap(Console.printLine(_))
}

/** Exercise 2: pick 3 environment variables sharing a common prefix, you can
  * imagine the prefix identifies some kind of hierarchy. Define a `Config`
  * parser that uses nesting appropriately.
  */
object Config2 extends ZIOAppDefault {
  case class Config(value1: String, values2: List[String], value3: String)

  val value1: ConfigDescriptor[String] =
    ConfigDescriptor.string("NODE") ?? "A node"

  val value2 =
    ConfigDescriptor.nested("EXTRA")(
      ConfigDescriptor.list("ARGS")(ConfigDescriptor.string)
    )

  val value3 =
    ConfigDescriptor.string("MAIN")

  val config: ConfigDescriptor[Config] =
    ConfigDescriptor
      .nested("VSCODE")(
        ConfigDescriptor.nested("GIT")(
          ConfigDescriptor.nested("ASKPASS")(
            value1.zip(value2).zip(value3)
          )
        )
      )
      .to[Config]

  val source =
    ConfigSource
      .fromSystemEnv(keyDelimiter = Some('_'), valueDelimiter = Some(' '))
      .orElse(
        ConfigSource.fromMap(
          Map("VSCODE.GIT.ASKPASS.NODE" -> "hello"),
          keyDelimiter = Some('.')
        )
      )

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    read(config.from(source))
      .flatMap(Console.printLine(_))
}

/** Exercise 3: introduce a new configuration descriptor that parses a
  * filesystem path.
  */
object Config3 extends ZIOAppDefault {
  case class Config(value1: Path, values2: List[String], value3: Path)

  def path(path: String): ConfigDescriptor[Path] =
    ???

  val config: ConfigDescriptor[Config] =
    ???

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    read(
      config from ConfigSource.fromSystemEnv(
        keyDelimiter = Some('_'),
        valueDelimiter = Some(' ')
      )
    )
      .flatMap(Console.printLine(_))
}

/** Exercise 4: play around with the exercises above to support the following:
  *
  *   - optional configuration
  *   - lists of values
  *   - optional configuration with default
  *   - documentation
  */
