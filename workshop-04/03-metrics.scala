//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-metrics-connectors:2.0.0"
//> using lib "io.d11::zhttp:2.0.0-RC11"

package workshop4

import zio._
import zio.metrics._

/** Exercise1. Write an implementation of the service interface that counts all
  * calls to the query endpoint.
  */
object Metrics1 extends ZIOAppDefault {

  trait Service {
    def query(request: Int): UIO[String]
  }

  object Service {
    def query(request: Int): URIO[Service, String] =
      ZIO.serviceWithZIO[Service](_.query(request))

    val queryCount: Metric.Counter[Any] =
      Metric.counterInt("query").fromConst(1)

    val Live: ZLayer[Any, Nothing, Service] =
      ZLayer.succeed((request: Int) =>
        ZIO
          .succeed("Hello")
          .delay(1.second) @@ queryCount @@ ZIOAspect.logged @@ ZIOAspect
          .annotated("clientId" -> "7000")
      )
  }

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    Service.query(42).provide(Service.Live)
}

/** Exercise2. Using the previous exerise as a blueprint, experience the other
  * types of metrics.
  */
object Metrics2 extends ZIOAppDefault {
  trait Service {
    def query(request: Int): UIO[String]
  }

  object Service {
    def query(request: Int): URIO[Service, String] =
      ZIO.serviceWithZIO[Service](_.query(request))

    val queryCount: Metric.Counter[Any] =
      Metric.counterInt("query").fromConst(1)

    val Live: ZLayer[Any, Nothing, Service] =
      ZLayer.succeed((request: Int) =>
        ZIO
          .succeed("Hello")
          .delay(1.second) @@ queryCount @@ ZIOAspect.logged @@ ZIOAspect
          .annotated("clientId" -> "7000")
      )
  }

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    Service.query(42).provide(Service.Live)
}

/** Exercise3. Expose metrics to Prometheus using zio-metrics-connectors.
  */
object Metrics3 extends ZIOAppDefault {
  import zio.metrics.connectors.{MetricsConfig, prometheus}
  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] = ???

}
