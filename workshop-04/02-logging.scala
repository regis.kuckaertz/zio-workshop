//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-logging:2.1.0"
//> using lib "dev.zio::zio-logging-slf4j:2.1.0"
//> using lib "org.slf4j:slf4j-simple:2.0.0"

package workshop4

import zio._
import zio.logging.backend.SLF4J
import zio.logging.LogFormat

/** Exercise 1. Run the program below and observe the output. What is the
  * result?
  */
object Logging1 extends ZIOAppDefault {
  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    ZIO.logAnnotate("regis", "hello") {
      for {
        _ <- ZIO.log("Hello World")
        _ <- ZIO.logTrace("Hello World")
        _ <- ZIO.logDebug("Hello World")
        _ <- ZIO.logInfo("Hello World")
        _ <- ZIO.logWarning("Hello World")
        _ <- ZIO.logError("Hello World")
        _ <- ZIO.logFatal("Hello World")
        _ <- boom()
      } yield ()
    }

  def boom() =
    ZIO.never.fork
      .flatMap(_.interrupt)
      .flatMap(exit =>
        exit.causeOption.fold(ZIO.unit)(ZIO.logErrorCause("Oh noes", _))
      )
}

/** Exercise 2. Write an implementation of the service interface below that
  * annotates each action with the client Id.
  */
object Logging2 extends ZIOAppDefault {
  final case class ClientId(getClientId: Long) extends AnyVal

  trait Service {
    def query(request: Int): URIO[ClientId, String]
  }

  object Service {
    def query(request: Int): URIO[Service & ClientId, String] =
      ZIO.serviceWithZIO[Service](_.query(request))

    def Live: ZLayer[Any, Nothing, Service] =
      ZLayer.succeed { (request: Int) =>
        ZIO.logSpan("query") {
          (
            for {
              _ <- ZIO.log("Hello World")
              _ <- ZIO.logTrace("Hello World")
              _ <- ZIO.logDebug("Hello World")
              _ <- ZIO.logSpan("inner-query") {
                ZIO.logAnnotate("clientId", "24000") {
                  ZIO.logInfo("Hello World") *> ZIO.logWarning("Hello World")
                }
              }
              _ <- ZIO.logError("Hello World")
              _ <- ZIO.logFatal("Hello World")
            } yield "Hello World"
          ) @@ ZIOAspect.annotated("clientId", "7000")
        }
      }
  }

  val HSBC = ClientId(7000L)

  val program: URIO[ClientId, Any] =
    Service.query(42).provideSome[ClientId](Service.Live)

  override val bootstrap: ZLayer[ZIOAppArgs with Scope, Any, Any] =
    Runtime.removeDefaultLoggers ++
      Runtime.addLogger(SLF4J.slf4jLogger(LogFormat.colored, _ => "Logging2"))

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    program.provide(ZLayer.succeed(HSBC))
}

/** Exercise 3. Using the program above as a blueprint, introduce a random sleep
  * period of between 1 and 5 seconds in the service, then track the execution
  * time of the query in a span.
  */
object Logging3 extends ZIOAppDefault {
  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] = ???
}
