//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.1"
//> using lib "dev.zio::zio-streams:2.0.1"
//> using lib "dev.zio::zio-test:2.0.1"

package workshop3

import zio._
import java.net.URL

trait Http {
  def get(uri: URL): Task[String]
}

object Http {
  def get(uri: URL): RIO[Http, String] =
    ZIO.serviceWithZIO(_.get(uri))

  val test =
    ZLayer.fromZIO {
      ZIO.acquireRelease(
        ZIO.logInfo("Starting Http server") *> ZIO.succeed(new Http {
          def get(uri: URL): Task[String] = ZIO.succeed(uri.toString())
        })
      )(_ => ZIO.logInfo("Stopping Http server"))
    }
}

object Module1 extends ZIOApp {
  type Environment = Http

  val environmentTag = EnvironmentTag[Http]

  def bootstrap = Http.test

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    ZIO.logInfo("Hello Module1") *> Http
      .get(new URL("http://google.com"))
      .flatMap(ZIO.logWarning(_))
}

object Module2 extends ZIOAppDefault {

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    ZIO.logInfo("Hello Module2")
}

object Main extends ZIOApp.Proxy(Module1 <> Module2)
