//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.1"
//> using lib "dev.zio::zio-streams:2.0.1"
//> using lib "dev.zio::zio-test:2.0.1"

package workshop3

import zio._

object Schedule1 extends ZIOAppDefault {

  // a schedule that repeats 5 times
  def s1 = Schedule.recurs(5)

  // a schedule that repeats 5 times every 200ms
  def s2 = Schedule.recurs(5) && Schedule.spaced(200.milli)

  // a schedule that repeats 5 times every 200ms then 5 times every 400ms
  def s3 = ???

  // a schedule that repeats until the input is `true` for a maximum of 10 times, using an exponential delay with jitter
  def s4 = ???

  // a schedule that repeats every day of the week (mon-fri) at 09:05, 13:00, 16:30, 17:15, 17:45, 18:15
  def weekdays: Schedule[Any, Any, (Long, Long, Long, Long, Long)] =
    Schedule.dayOfWeek(1) ||
      Schedule.dayOfWeek(2) ||
      Schedule.dayOfWeek(3) ||
      Schedule.dayOfWeek(4) ||
      Schedule.dayOfWeek(5)

  def hours =
    Schedule.hourOfDay(9) && Schedule.minuteOfHour(5) ||
      Schedule.hourOfDay(13) && Schedule.minuteOfHour(0)

  def s5 = weekdays && hours

  def program(state: Ref[Int]): UIO[Boolean] =
    state.modify {
      case n if n < 5 => (false, n + 1)
      case n          => (true, n)
    }

  def run: ZIO[Environment with ZIOAppArgs with Scope, Any, Any] =
    for {
      args <- getArgs
      _ <- Console.printLine(args.mkString(","))
      state <- Ref.make(0)
      _ <- program(state)
        .repeat(Schedule.recurs(5))
        .repeat(Schedule.spaced(1.second))
        .repeat(
          s2.tapInput(x => ZIO.logInfo(x.toString()))
        )
        .timeout(10.seconds)
    } yield ()
}
