//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.1"
//> using lib "dev.zio::zio-streams:2.0.1"
//> using lib "dev.zio::zio-test:2.0.1"

package workshop3

import zio._
import zio.stream._

trait Thunk {
  def taskId: Int
}

trait Result {
  def result: Int
}

/** Write an application that delegates tasks to a pool of workers. The pool has
  * a minimum of 10 workers but can scale up to 100 when demands increases. A
  * worker is automatically torn down if inactive after 5 seconds.
  *
  * Log all activity surrounding workers.
  *
  * Every action involving workers should ideally log the worker id.
  */
object Pool1 extends ZIOApp {
  type WorkerId = Int

  trait Worker {
    def workerId: Int

    def execute(thunk: Thunk): Task[Result]
  }

  object Worker {
    def acquire: URIO[ZState[WorkerId], Worker] =
      for {
        workerId0 <- ZIO.getState[WorkerId]
        _ <- ZIO.updateState[WorkerId](_ + 1)
      } yield new Worker {
        val workerId = workerId0

        def execute(thunk: Thunk) =
          ZIO.logAnnotate("worker-id", workerId.toString) {
            ZIO
              .logInfo(s"Executing task ${thunk.taskId}")
              .as(new Result { val result = thunk.taskId })
              .delay(500.millis)
          }
      }

    def release(w: Worker): UIO[Unit] =
      ZIO.logWarning(s"Releasing ${w.workerId}")
  }

  type Environment =
    ZState[WorkerId] & ZPool[ZState[WorkerId], Worker]

  val environmentTag: EnvironmentTag[Environment] =
    EnvironmentTag[ZState[WorkerId] & ZPool[ZState[WorkerId], Worker]]

  override val bootstrap: ZLayer[
    Any with Scope,
    Nothing,
    ZState[Int] & ZPool[ZState[WorkerId], Worker]
  ] =
    ZState
      .initial(0)
      .>+>(
        ZLayer.fromZIO(
          ZPool.make(
            ZIO.acquireRelease(Worker.acquire)(Worker.release),
            1 to 10,
            5.seconds
          )
        )
      )

  val run =
    for {
      pool <- ZIO.service[ZPool[ZState[WorkerId], Worker]]
      _ <- ZStream
        .fromIterable(0 to 100)
        .mapZIOPar(10)(id =>
          ZIO.scoped {
            for {
              worker <- pool.get
              _ <- worker.execute(new Thunk { val taskId = id })
            } yield ()
          }
        )
        .runDrain
    } yield ()
}
