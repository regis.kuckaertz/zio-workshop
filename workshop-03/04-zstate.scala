//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.1"
//> using lib "dev.zio::zio-streams:2.0.1"
//> using lib "dev.zio::zio-test:2.0.1"

package workshop3

import zio._

case class State[s, a](runState: s => (s, a)) {
  def map[b](f: a => b): State[s, b] =
    State { s =>
      val (s1, a) = runState(s)
      (s1, f(a))
    }

  def flatMap[b](f: a => State[s, b]): State[s, b] =
    State { s =>
      val (s1, a) = runState(s)
      f(a).runState(s1)
    }
}

object State {
  def apply[s, a](initial: a): State[s, a] =
    State(s => (s, initial))

  def get[s]: State[s, s] =
    State(s => (s, s))

  def set[s](s: s): State[s, Unit] =
    State(_ => (s, ()))

  def update[s](f: s => s): State[s, s] =
    for {
      s <- State.get[s]
      s1 = f(s)
      _ <- State.set(s1)
    } yield s1
}
