//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-streams:2.0.2"
//> using lib "dev.zio::zio-test:2.0.2"
//> using lib "io.github.scottweaver::zio-2-0-testcontainers-kafka:0.9.0"

package workshop5

import zio.test._
import zio.stream.ZStream
import zio._
import zio.kafka.producer.ProducerSettings
import zio.kafka.consumer.ConsumerSettings
import io.github.scottweaver.zio.testcontainers.kafka.ZKafkaContainer
import com.dimafeng.testcontainers.KafkaContainer

abstract class KafkaSpec extends ZIOSpec[ProducerSettings & ConsumerSettings] {
  override val bootstrap
      : ZLayer[Scope, Any, ProducerSettings with ConsumerSettings] =
    ZKafkaContainer.Settings.default >>> ZKafkaContainer.live >>> (
      ZKafkaContainer.defaultConsumerSettings ++
        ZKafkaContainer.defaultProducerSettings
    )
}

object Test3 extends KafkaSpec {
  def spec = test("test3") {
    assertTrue(1 == 1)
  }
}

object Test31 extends KafkaSpec {
  def spec = test("test31") {
    assertTrue(1 == 1)
  }
}
