//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-streams:2.0.2"
//> using lib "dev.zio::zio-test:2.0.2"

package workshop5

import zio.test._
import zio.stream.ZStream

object Test1 extends ZIOSpecDefault {
  lazy val fibs: LazyList[Int] =
    0 #:: 1 #:: fibs.zip(fibs.tail).map { case (a, b) => a + b }

  def spec = test("test1") {
    assertTrue(1 == 1)
  }
}
