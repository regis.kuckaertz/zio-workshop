//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-streams:2.0.2"
//> using lib "dev.zio::zio-test:2.0.2"
//> using lib "dev.zio::zio-mock:1.0.0-RC9"

import zio._
import zio.test.{test, _}
import zio.mock._
import zio.stream._

object Test5 extends ZIOSpecDefault {
  trait ServiceA {
    def a: UIO[Int]
  }

  object ServiceA {
    val live =
      ZLayer {
        for {
          serviceB <- ZIO.service[ServiceB]
        } yield new ServiceA {
          def a: UIO[Int] = serviceB.b(false, 1.0d).map(_.length())
        }
      }
  }

  trait ServiceB {
    def b(x: Boolean, y: Double): UIO[String]
    def bs(x: List[String]): UStream[Float]
  }

  object ServiceBMock extends Mock[ServiceB] {
    object B extends Effect[(Boolean, Double), Nothing, String]
    object BS extends Stream[List[String], Nothing, Float]

    val compose =
      ZLayer {
        for {
          proxy <- ZIO.service[Proxy]
        } yield new ServiceB {
          def b(x: Boolean, y: Double): UIO[String] = proxy(B, x, y)
          def bs(x: List[String]): UStream[Float] = ZStream.unwrap(proxy(BS, x))
        }
      }
  }

  def spec = test("test5") {
    assertTrue(1 == 1)
  }
}
