//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-streams:2.0.2"
//> using lib "dev.zio::zio-test:2.0.2"

package workshop5

import zio.test._
import zio.stream.ZStream
import zio.ZLayer
import zio.Ref
import zio.ZIO

object Test2 extends ZIOSpecDefault {
  val sharedLayer = ZLayer(Ref.make(0))

  val spec = ???
}
