//> using scala "2.13"
//> using lib "dev.zio::zio:2.0.2"
//> using lib "dev.zio::zio-test:2.0.2"

package workshop5

import zio.test._
import zio._

object Test4 extends ZIOSpecDefault with Layers {
  def spec =
    test("This program terminates") {
      assertZIO(ZIO.never)(Assertion.anything)
    } @@ TestAspect.timeout(10.second)
}

trait Layers {
  type Table[a] = String

  trait Datastore {
    def insert[a](table: Table[a], items: List[a]): UIO[List[Int]]
    def delete[a](table: Table[a], ids: Option[List[Int]]): UIO[Unit]
    def getById[a](table: Table[a], id: Int): UIO[Option[a]]
    def getAll[a](table: Table[a]): UIO[List[a]]
  }

  object Datastore {
    val layer =
      ZLayer {
        for {
          tables <- Ref.make(Map.empty[Table[_], Map[Int, _]])
        } yield new Datastore {
          def delete[a](table: Table[a], ids: Option[List[Int]]): UIO[Unit] =
            for {
              _ <- ZIO.logInfo(s"""Deleting ${ids
                  .map(_.mkString)
                  .getOrElse("all")} from $table""")
              _ <- tables.update { t =>
                t.get(table) match {
                  case Some(items) =>
                    ids match {
                      case Some(ids) =>
                        t.updated(table, items -- ids)
                      case None =>
                        t - table
                    }
                  case None => t
                }
              }
            } yield ()

          def getAll[a](table: Table[a]): UIO[List[a]] =
            tables.get.map(
              _.get(table)
                .map(_.values.toList.asInstanceOf[List[a]])
                .getOrElse(Nil)
            )

          def getById[a](table: Table[a], id: Int): UIO[Option[a]] =
            tables.get.map(
              _.get(table)
                .flatMap(_.get(id))
                .map(_.asInstanceOf[a])
            )

          def insert[a](table: Table[a], items: List[a]): UIO[List[Int]] =
            tables
              .modify { t =>
                val ids = t.get(table) match {
                  case Some(items) =>
                    items.keys.max + 1 to items.keys.max + items.size
                  case None =>
                    1 to items.size
                }
                ids.toList -> t.updated(
                  table,
                  t.get(table).getOrElse(Map.empty) ++ ids.zip(items)
                )
              }
        }
      }
  }

}
