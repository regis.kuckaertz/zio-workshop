# ZIO workshops

A collection of themed exercises around the ZIO library and its ecoystem.

## Usage instructions

1. Fork this repository, you will work from your own fork
2. Setup the project in any way you want. I will use scala-cli myself, but you are free to use SBT or anything you like, I don't mind.

All the script files will be using:

- ZIO 2.0 and libraries relying on ZIO 2.0
- Scala 2.13

Let's get started 🚀